USE [master]
GO
/****** Object:  Database [GENERALA]    Script Date: 17/11/2020 21:14:03 ******/
CREATE DATABASE [GENERALA]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'GENERALA', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\GENERALA.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'GENERALA_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\GENERALA_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [GENERALA] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [GENERALA].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [GENERALA] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [GENERALA] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [GENERALA] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [GENERALA] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [GENERALA] SET ARITHABORT OFF 
GO
ALTER DATABASE [GENERALA] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [GENERALA] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [GENERALA] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [GENERALA] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [GENERALA] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [GENERALA] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [GENERALA] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [GENERALA] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [GENERALA] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [GENERALA] SET  DISABLE_BROKER 
GO
ALTER DATABASE [GENERALA] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [GENERALA] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [GENERALA] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [GENERALA] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [GENERALA] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [GENERALA] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [GENERALA] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [GENERALA] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [GENERALA] SET  MULTI_USER 
GO
ALTER DATABASE [GENERALA] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [GENERALA] SET DB_CHAINING OFF 
GO
ALTER DATABASE [GENERALA] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [GENERALA] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [GENERALA] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [GENERALA] SET QUERY_STORE = OFF
GO
USE [GENERALA]
GO
/****** Object:  Table [dbo].[JUGADOR]    Script Date: 17/11/2020 21:14:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JUGADOR](
	[ID] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[TiempoJugado] [time](7) NOT NULL,
	[Clave] [varchar](50) NOT NULL,
 CONSTRAINT [PK_JUGADOR] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JUGADOR-PARTIDA]    Script Date: 17/11/2020 21:14:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JUGADOR-PARTIDA](
	[ID_Jugador] [int] NOT NULL,
	[ID_Partida] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PARTIDA]    Script Date: 17/11/2020 21:14:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PARTIDA](
	[ID] [int] NOT NULL,
	[GANADOR] [int] NULL,
	[EMPATE] [bit] NULL,
	[INICIO] [varchar](50) NOT NULL,
	[FINAL] [varchar](50) NULL,
 CONSTRAINT [PK_PARTIDA] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[JUGADOR-PARTIDA]  WITH CHECK ADD  CONSTRAINT [FK_JUGADOR-PARTIDA_JUGADOR] FOREIGN KEY([ID_Jugador])
REFERENCES [dbo].[JUGADOR] ([ID])
GO
ALTER TABLE [dbo].[JUGADOR-PARTIDA] CHECK CONSTRAINT [FK_JUGADOR-PARTIDA_JUGADOR]
GO
ALTER TABLE [dbo].[JUGADOR-PARTIDA]  WITH CHECK ADD  CONSTRAINT [FK_JUGADOR-PARTIDA_PARTIDA] FOREIGN KEY([ID_Partida])
REFERENCES [dbo].[PARTIDA] ([ID])
GO
ALTER TABLE [dbo].[JUGADOR-PARTIDA] CHECK CONSTRAINT [FK_JUGADOR-PARTIDA_PARTIDA]
GO
ALTER TABLE [dbo].[PARTIDA]  WITH CHECK ADD  CONSTRAINT [FK_PARTIDA_JUGADOR] FOREIGN KEY([GANADOR])
REFERENCES [dbo].[JUGADOR] ([ID])
GO
ALTER TABLE [dbo].[PARTIDA] CHECK CONSTRAINT [FK_PARTIDA_JUGADOR]
GO
/****** Object:  StoredProcedure [dbo].[AltaJugador]    Script Date: 17/11/2020 21:14:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[AltaJugador]

@Nombre varchar(50), @Clave varchar(50)

as
	declare @tiempo time(7)
	set @tiempo = TIMEFROMPARTS(0,0,0,0,0)   /*HORAS, MINUTOS, SEGUNDOS, ..., ...*/
	declare @id int
	set @id = isnull((select MAX(ID) from JUGADOR),0) + 1
	insert into JUGADOR(ID, Nombre, Clave, TiempoJugado)
	values (@id, @Nombre, @Clave, @tiempo) 
GO
/****** Object:  StoredProcedure [dbo].[AltaJugador_Partida]    Script Date: 17/11/2020 21:14:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[AltaJugador_Partida]

@ID_Jugador int, @ID_Partida int

as

	insert into [JUGADOR-PARTIDA] (ID_Jugador, ID_Partida)
	values (@ID_Jugador, @ID_Partida)

GO
/****** Object:  StoredProcedure [dbo].[AltaPartida]    Script Date: 17/11/2020 21:14:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[AltaPartida]

@Inicio varchar(50)

as

	declare @ID int
	set @ID = ISNULL((select MAX (ID) from PARTIDA),0) + 1

	insert into PARTIDA (ID, INICIO)
	values (@ID, @Inicio)
	
GO
/****** Object:  StoredProcedure [dbo].[BorrarJugadoresUltimaPartida]    Script Date: 17/11/2020 21:14:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[BorrarJugadoresUltimaPartida]

as

	declare @ID int
	set @ID = (select MAX (ID) from PARTIDA)
	
	delete from [JUGADOR-PARTIDA]
	where ID_Partida = @ID

GO
/****** Object:  StoredProcedure [dbo].[BorrarUltimaPartida]    Script Date: 17/11/2020 21:14:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[BorrarUltimaPartida]

as

	declare @ID int
	set @ID = (select MAX (ID) from PARTIDA)
	
	delete from PARTIDA
	where ID = @ID

GO
/****** Object:  StoredProcedure [dbo].[EstablecerGanadorPartida]    Script Date: 17/11/2020 21:14:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[EstablecerGanadorPartida]

@ID int, @Ganador int = null, @Empate bit = null, @Final varchar(50)

as
	
	if(@Empate = null)
	begin
		set @Empate = 0;
	end
	else
	begin
		set @Empate = 1
	end
	
	if(@Ganador = null)
	begin
		set @Ganador = 0;
	end

	update PARTIDA
	set GANADOR = @Ganador, EMPATE = @Empate, FINAL = @Final
	where ID = @ID

GO
/****** Object:  StoredProcedure [dbo].[IDPartida]    Script Date: 17/11/2020 21:14:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[IDPartida]

as
	select ISNULL((select MAX (ID) from PARTIDA),0)
GO
/****** Object:  StoredProcedure [dbo].[IniciarSesion]    Script Date: 17/11/2020 21:14:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[IniciarSesion]

@Nombre varchar(50), @Clave varchar(50)

as

select * from JUGADOR where Nombre = @Nombre and Clave = @Clave 

GO
/****** Object:  StoredProcedure [dbo].[ListarJugadores]    Script Date: 17/11/2020 21:14:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ListarJugadores]

as

select * from JUGADOR

GO
/****** Object:  StoredProcedure [dbo].[ListarJugadoresPorPartida]    Script Date: 17/11/2020 21:14:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ListarJugadoresPorPartida]

@ID_Partida int

as

	select * from [JUGADOR-PARTIDA]
	where ID_Partida = @ID_Partida

GO
/****** Object:  StoredProcedure [dbo].[ListarPartidas]    Script Date: 17/11/2020 21:14:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ListarPartidas]

as
	select * from PARTIDA
GO
/****** Object:  StoredProcedure [dbo].[ModificarJugador]    Script Date: 17/11/2020 21:14:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ModificarJugador]

@ID int, @Horas int, @Minutos int, @Segundos int

as
	declare @Tiempo time(7)
	set @Tiempo = TIMEFROMPARTS(@Horas, @Minutos, @Segundos, 0, 0)

	update JUGADOR
	set TiempoJugado = @Tiempo
	where JUGADOR.ID = @ID

GO
/****** Object:  StoredProcedure [dbo].[PartidasPorJugador]    Script Date: 17/11/2020 21:14:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[PartidasPorJugador]

@ID_Jugador int

as

	select * 
	from [JUGADOR-PARTIDA]
	where ID_Jugador = @ID_Jugador

GO
USE [master]
GO
ALTER DATABASE [GENERALA] SET  READ_WRITE 
GO
