﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using BE;


namespace DAL
{
    public class MP_JUGADOR
    {

        ACCESO acceso = new ACCESO();

        public void RegistrarNuevoJugador(string nombre, string clave)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@Nombre", nombre));
            parametros.Add(acceso.CrearParametro("@Clave", clave));

            acceso.Abrir();
            acceso.Escribir("AltaJugador", parametros);
            acceso.Cerrar();

        }

        static public BE.JUGADOR IniciarSesion(string nombre, string clave)
        {
            ACCESO acceso = new ACCESO();

            acceso.Abrir();

            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@Nombre", nombre));
            parametros.Add(acceso.CrearParametro("@Clave", clave));

            DataTable tabla = acceso.Leer("IniciarSesion", parametros);

            acceso.Cerrar();

            foreach (DataRow j in tabla.Rows)
            {
                if (j[1].ToString() == nombre)
                {
                    if (j[3].ToString() == clave)
                    {
                        
                                BE.JUGADOR jug = new JUGADOR();

                                jug.id = int.Parse(j[0].ToString());
                                jug.Nombre = nombre;
                                jug.TiempoJugado = TimeSpan.Parse(j[2].ToString());
                                return jug;
                           
                    }
                }
            }
            return null;
        }

        public void ModificarTiempo(BE.JUGADOR jugador)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@ID", jugador.id));
            parametros.Add(acceso.CrearParametro("@Horas", int.Parse(jugador.TiempoJugado.Hours.ToString())));
            parametros.Add(acceso.CrearParametro("@Minutos", int.Parse(jugador.TiempoJugado.Minutes.ToString())));
            parametros.Add(acceso.CrearParametro("@Segundos", int.Parse(jugador.TiempoJugado.Seconds.ToString())));

            acceso.Abrir();
                        
            acceso.Escribir("ModificarJugador", parametros);
            
            acceso.Cerrar();
        }

        internal static void AsignarPartida(BE.PARTIDA p)
        {
            p.Jugadores[0].Partidas.Add(p);
            p.Jugadores[1].Partidas.Add(p);

        }

        static public List<JUGADOR> ListarJugadores()
        {
            ACCESO acceso = new ACCESO();
            List<BE.JUGADOR> jugadores = new List<JUGADOR>();

            acceso.Abrir();

            DataTable tabla = new DataTable();
            tabla = acceso.Leer("ListarJugadores");

            acceso.Cerrar();


            foreach (DataRow r in tabla.Rows)
            {
                BE.JUGADOR j = new JUGADOR();

                j.id = int.Parse(r[0].ToString());
                j.Nombre = r[1].ToString();
                j.TiempoJugado = TimeSpan.Parse(r[2].ToString());

                jugadores.Add(j);
            }

            return jugadores;
        }


    }
}
