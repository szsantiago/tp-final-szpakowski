﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BE;

namespace DAL
{
    internal class MP_JUGADOR_PARTIDA
    {
        

        static public void Alta(BE.PARTIDA p)
        {
            ACCESO acceso = new ACCESO();

            List<SqlParameter> parametros = new List<SqlParameter>();
                        
            foreach (BE.JUGADOR j in p.Jugadores)
            {
                parametros.Clear();
                parametros.Add(acceso.CrearParametro("@ID_Jugador", j.id));
                parametros.Add(acceso.CrearParametro("@ID_Partida", p.ID));
                acceso.Abrir();
                acceso.Escribir("AltaJugador_Partida", parametros);
                acceso.Cerrar();
            }
        }

        static public List<BE.JUGADOR> JugadoresPorPartida(BE.PARTIDA p)
        {
            ACCESO acceso = new ACCESO();
            List<BE.JUGADOR> jugadores = MP_JUGADOR.ListarJugadores();
            List<BE.JUGADOR> jugadoresPartida = new List<JUGADOR>();

            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@ID_Partida", p.ID));

            acceso.Abrir();
            DataTable tabla = new DataTable();
            tabla = acceso.Leer("ListarJugadoresPorPartida", parametros);
            acceso.Cerrar();
            

            foreach (DataRow r in tabla.Rows)
            {
                if (p.ID == int.Parse(r[1].ToString()))
                {
                    foreach (BE.JUGADOR juga in jugadores)
                    {
                        if (int.Parse(r[0].ToString()) == juga.id)
                        {
                            jugadoresPartida.Add(juga);
                        }
                    }
                }
                    
                
            }

            return jugadoresPartida;
        }
    }
}
