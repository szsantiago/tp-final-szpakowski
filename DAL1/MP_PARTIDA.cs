﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using BE;

namespace DAL
{
    public class MP_PARTIDA
    {
        ACCESO acceso = new ACCESO();

        public void NuevaPartida(BE.PARTIDA p)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("Inicio", p.Inicio.ToString()));

            acceso.Abrir();
            acceso.Escribir("AltaPartida", parametros);
            p.ID = acceso.LeerEscalar("IDPartida");
            acceso.Cerrar();
                        
            MP_JUGADOR_PARTIDA.Alta(p);

            MP_JUGADOR.AsignarPartida(p);

        }

        public void FinalPartida(BE.PARTIDA p)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@ID", p.ID));

            if (p.Ganador == null)
            {
                parametros.Add(acceso.CrearParametro("@Empate", 1));
            }
            else
            {
                parametros.Add(acceso.CrearParametro("@Ganador", p.Ganador.id));
            }

            parametros.Add(acceso.CrearParametro("@Final", p.Final.ToString()));

            acceso.Abrir();
            acceso.Escribir("EstablecerGanadorPartida", parametros);
            acceso.Cerrar();

        }

        public void BorrarUltimaPartida()
        {
            acceso.Abrir();
            acceso.Escribir("BorrarJugadoresUltimaPartida");
            acceso.Cerrar();
            acceso.Abrir();
            acceso.Escribir("BorrarUltimaPartida");
            acceso.Cerrar();
        }

        static public List<BE.PARTIDA> ListarPartidas()  
        {
            ACCESO ac = new ACCESO();

            List<BE.PARTIDA> partidas = new List<PARTIDA>();

            DataTable datos = new DataTable();

            ac.Abrir();
            datos = ac.Leer("ListarPartidas");
            ac.Cerrar();

            List<BE.JUGADOR> jugadores = MP_JUGADOR.ListarJugadores();

            foreach (DataRow part in datos.Rows)
            {
                PARTIDA p = new PARTIDA();

                p.ID = int.Parse(part[0].ToString());

                if (part[1].ToString() != "")
                {
                    foreach (BE.JUGADOR j in jugadores)
                    {
                        if (int.Parse(part[1].ToString()) == j.id)
                        {
                            p.Ganador = j;
                        }
                    }
                }
                else
                {
                    p.Ganador = null;
                }

                p.Inicio = DateTime.Parse(part[3].ToString());

                if (part[4].ToString() != "")
                {
                    p.Final = DateTime.Parse(part[4].ToString());
                }
                               

                p.Jugadores = MP_JUGADOR_PARTIDA.JugadoresPorPartida(p);
                
                partidas.Add(p);
            }

            return partidas;
        }

    }
}
