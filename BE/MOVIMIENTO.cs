﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class MOVIMIENTO
    {

        private JUGADOR jugador;

        public JUGADOR JUGADOR
        {
            get { return jugador; }
            set { jugador = value; }
        }

        private PARTIDA partida;

        public PARTIDA PARTIDA
        {
            get { return partida; }
            set { partida = value; }
        }

        private PUNTAJE puntos;

        public PUNTAJE Puntos
        {
            get { return puntos; }
            set { puntos = value; }
        }


    }
}