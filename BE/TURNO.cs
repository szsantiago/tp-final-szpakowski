﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class TURNO
    {
        private PUNTAJE puntaje;

        public PUNTAJE PUNTAJE
        {
            get { return puntaje; }
            set { puntaje = value; }
        }
        
        private JUGADOR jugador;

        public JUGADOR JUGADOR
        {
            get { return jugador; }
            set { jugador = value; }
        }
        
        
        private int tirosRestantes = 3;

        public int TirosRestantes
        {
            get { return tirosRestantes; }
            set { tirosRestantes = value; }
        }

     

    }
}