﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    static public class TABLERO
    {
        public static Random random = new Random();

        static private List<DADO> dados = new List<DADO>();

        static public List<DADO> Dados
        {
            get { return dados; }
            set { dados = value; }
        }
    }
}