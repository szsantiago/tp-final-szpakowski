﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BE
{
    public class PARTIDA
    {

        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }


        private List<JUGADOR> jugadores = new List<JUGADOR>();

        public List<JUGADOR> Jugadores
        {
            get { return jugadores; }
            set { jugadores = value; }
        }

        private DateTime inicio;

        public DateTime Inicio
        {
            get { return inicio; }
            set { inicio = value; }
        }

        private DateTime final;

        public DateTime Final
        {
            get { return final; }
            set { final = value; }
        }

        private JUGADOR ganador;
            
        public JUGADOR Ganador
        {
            get { return ganador; }
            set { ganador = value; }
        }
        
        
        

    }
}