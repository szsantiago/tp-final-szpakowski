﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class JUGADOR
    {
        private int ID;

        public int id
        {
            get { return ID; }
            set { ID = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private TimeSpan tiempojugado = TimeSpan.Zero;

        public TimeSpan TiempoJugado
        {
            get { return tiempojugado; }
            set { tiempojugado = value; }
        }

        private int puntos;

        public int Puntos
        {
            get { return puntos; }
            set { puntos = value; }
        }

        private List<PARTIDA> partidas = new List<PARTIDA>();
    
        public List<PARTIDA> Partidas
        {
            get { return partidas; }
            set { partidas = value; }
        }

        private float porcentajeGanadas;

        public float Porcentaje_Ganadas
        {
            get { return porcentajeGanadas; }
            set { porcentajeGanadas = value; }
        }

    }
}