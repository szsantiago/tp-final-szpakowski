﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using BE;

namespace GENERALA
{
    public partial class Form1 : Form
    {
        FormPrincipal FP;
        static BE.TURNO turno = new BE.TURNO();
        BLL.JUGADOR j = new BLL.JUGADOR();
        BE.PARTIDA partida = new BE.PARTIDA();
        BLL.PARTIDA p = new BLL.PARTIDA();

        public Form1(BE.PARTIDA pa, FormPrincipal F)
        {
            InitializeComponent();
            FP = F;
            partida = pa;

            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            j.EnviarDado += T_EnviarDado;

            EmpezarPartida();
        }

        private void EmpezarPartida()
        {
            partida.ID = (BLL.PARTIDA.ListaPartidas().Count == 0) ? 1 : BLL.PARTIDA.ListaPartidas().Last().ID + 1;

            tablA_PUNTOS1.Jugadores = partida.Jugadores;

            turno.JUGADOR = partida.Jugadores[0];
            turno.TirosRestantes = 3;

            tablA_PUNTOS1.Turno = turno;

            label3.Text = "TURNO: " + tablA_PUNTOS1.Jugadores[0].Nombre;

            p.AltaPartida(partida);

            tablA_PUNTOS1.Partida = partida;
        }
        
        private void T_EnviarDado(List<BE.DADO> d)
        {

            foreach (BE.DADO DADO in d)
            {
                DADO dado = new DADO();
                dado.Width = 80;
                dado.Height = 80;
                dado.Location = new Point((DADO.POSICION.X * dado.Width), DADO.POSICION.Y + 100);
                dado.EstablecerImagen(DADO.Valor);
                dado.DadoAsignado = DADO;

                this.Controls.Add(dado);
            }

        }

        private void BorrarPictureBox()
        {
            int indice = 0;

            List<Control> controles_Borrar = new List<Control>();

            tablA_PUNTOS1.MostrarBotones();

            foreach (Control c in Controls)
            {
                if (c is DADO)
                {
                    controles_Borrar.Add(c);
                }
                else
                {
                    indice++;
                }
            }

            foreach (Control c in controles_Borrar)
            {
                Controls.RemoveAt(indice);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            BorrarPictureBox();

            j.Tirar();
            turno.TirosRestantes--;

            if (turno.TirosRestantes == 0)
            {
                button1.Enabled = false;
            }

            

            tablA_PUNTOS1.MostrarOpcionesPuntaje(BE.TABLERO.Dados, turno.TirosRestantes);

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (tablA_PUNTOS1.TerminoTurno)
            {
                turno = new BE.TURNO();
                turno.TirosRestantes = 3;
                BorrarPictureBox();
                tablA_PUNTOS1.EsconderBotones();

                if (tablA_PUNTOS1.Turno.JUGADOR == partida.Jugadores[0])
                {
                    tablA_PUNTOS1.Turno.JUGADOR = partida.Jugadores[1];
                    label3.Text = "TURNO: " + partida.Jugadores[1].Nombre;
                }
                else
                {
                    tablA_PUNTOS1.Turno.JUGADOR = partida.Jugadores[0];
                    label3.Text = "TURNO: " + partida.Jugadores[0].Nombre;
                }

                tablA_PUNTOS1.TerminoTurno = false;
                button1.Enabled = true;
            }

            if (tablA_PUNTOS1.VerificarCompleto())
            {
                j.ModificarTiempo(partida.Jugadores[0]);
                j.ModificarTiempo(partida.Jugadores[1]);
                timer1.Enabled = false;
                GandadorPartida();

            }
        }

        

        private void GandadorPartida(bool e = false)
        {   
            if (tablA_PUNTOS1.Jugadores[0].Puntos > tablA_PUNTOS1.Jugadores[1].Puntos)
            {
                partida.Ganador = tablA_PUNTOS1.Jugadores[0];
            }
            else if (tablA_PUNTOS1.Jugadores[0].Puntos < tablA_PUNTOS1.Jugadores[1].Puntos)
            {
                partida.Ganador = tablA_PUNTOS1.Jugadores[1];   
            }
            else
            {
                partida.Ganador = null;
            }

            if (!e)
            {
                if (partida.Ganador != null)
                {
                    MessageBox.Show("GANO: " + partida.Ganador.Nombre);
                }
                else
                {
                    MessageBox.Show("EMPATE");
                }
            }

            

            partida.Final = DateTime.Now;

            p.FinalPartida(partida);

            foreach (BE.JUGADOR j in partida.Jugadores)
            {
                j.Puntos = 0;
            }

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (partida.Ganador == null)
            {
                BLL.PARTIDA part = new BLL.PARTIDA();
                part.BorrarUltimaPartida();
            }

            FP.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (partida.Ganador == null)
            {
                BLL.PARTIDA part = new BLL.PARTIDA();
                part.BorrarUltimaPartida();
            }

            FP.AbrirFP();

            FP.Show();
            this.Close();

            
        }
        
    }
}




            
