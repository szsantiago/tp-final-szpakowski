﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using BLL;

namespace GENERALA
{
    public partial class FormESTADISTICAScs : Form
    {
        List<BE.JUGADOR> jugadores;

        public FormESTADISTICAScs()
        {
            InitializeComponent();
        }

        private void FormESTADISTICAScs_Load(object sender, EventArgs e)
        {
            dataGridView1.Columns.Add("U", "USUARIO");
            dataGridView1.Columns.Add("TJ", "TIEMPO JUGADO");
            dataGridView1.Columns.Add("P", "PARTIDAS");
            dataGridView1.Columns.Add("G", "GANADAS");
            dataGridView1.Columns.Add("E", "EMPATADS");
            dataGridView1.Columns.Add("PER", "PERDIDAS");
            dataGridView1.Columns.Add("PROM", "PROMEDIO VICTORIAS");

            jugadores = BLL.JUGADOR.ListarJugadores();

            foreach (BE.JUGADOR j in jugadores)
            {
                List<int> estadpart = BLL.JUGADOR.PartidasGanadasEmpatadasPerdidas(j);
                
                dataGridView1.Rows.Add(j.Nombre, j.TiempoJugado, estadpart[3].ToString(), estadpart[0].ToString(),
                                 estadpart[1].ToString(), estadpart[2].ToString(), j.Porcentaje_Ganadas.ToString("F2"));
            }
        }
    }
}
