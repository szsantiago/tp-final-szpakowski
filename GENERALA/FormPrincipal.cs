﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GENERALA
{
    public partial class FormPrincipal : Form
    {
        bool elementos = true;

        public FormPrincipal()
        {
            InitializeComponent();
        }

        private void FormPrincipal_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            dataGridView1.DataSource = null;
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count == 2)
            {
                BE.PARTIDA partida = new BE.PARTIDA();

                partida.Inicio = DateTime.Now;
                partida.Jugadores.Add((BE.JUGADOR)dataGridView1.Rows[0].DataBoundItem);
                partida.Jugadores.Add((BE.JUGADOR)dataGridView1.Rows[1].DataBoundItem);

                //timer1.Enabled = false;
                timer2.Interval = 1000;
                timer2.Enabled = true;

                Form1 F1 = new Form1(partida, this);
                F1.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("FALTAN JUGADORES");
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {                        
            if (iniciarSesion1.Jugadores != null && iniciarSesion1.Jugadores.Count == 1 && elementos)
            {
                elementos = false;
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = iniciarSesion1.Jugadores;
                
            }
            else if (iniciarSesion1.Jugadores != null && iniciarSesion1.Jugadores.Count == 2 && !elementos)
            {
                elementos = true;
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = iniciarSesion1.Jugadores;
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            TimeSpan ts = new TimeSpan(00, 00, 01);

            BE.JUGADOR jug = (BE.JUGADOR)dataGridView1.Rows[0].DataBoundItem;                            
            jug.TiempoJugado = jug.TiempoJugado.Add(ts);

            jug = (BE.JUGADOR)dataGridView1.Rows[1].DataBoundItem;
            jug.TiempoJugado = jug.TiempoJugado.Add(ts);                          
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormESTADISTICAScs es = new FormESTADISTICAScs();
            es.ShowDialog();
        }

        public void AbrirFP()
        {
            dataGridView1.DataSource = null;
            timer2.Enabled = false;
            iniciarSesion1.Limpiar(true);
        }
    }
}
