﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GENERALA
{
    public partial class TABLA_PUNTOS : UserControl
    {
        public TABLA_PUNTOS()
        {
            InitializeComponent();
        }

        private BE.PARTIDA partida;

        public BE.PARTIDA Partida
        {
            get { return partida; }
            set { partida = value; }
        }


        private List<BE.JUGADOR> jugadores = new List<BE.JUGADOR>();

        public List<BE.JUGADOR> Jugadores
        {
            get { return jugadores; }
            set { jugadores = value; }
        }

        private BE.TURNO turno;

        public BE.TURNO Turno
        {
            get { return turno; }
            set { turno = value; }
        }

        private bool terminoTurno = false;

        public bool TerminoTurno
        {
            get { return terminoTurno; }
            set { terminoTurno = value; }
        }

        private bool completo = false;

        public bool Completo
        {
            get { return completo; }
            set { completo = value; }
        }

        private void TABLA_PUNTOS_Load(object sender, EventArgs e)
        {
            EsconderTextbox();

            EsconderBotones();
        }

        public void EsconderTextbox()
        {
            foreach (Control c in Controls)
            {
                if (c is TextBox)
                {
                    c.Text = "0";
                    c.Visible = false;
                }
                if (c is Label)
                {
                    c.Text = "0";
                }
            }
        }

        public void EsconderBotones()
        {
            foreach (Control c in Controls)
            {
                if (c is Button)
                {
                    c.Visible = false;
                }
            }
        }
        
        public void MostrarBotones()
        {
            foreach (Control c in Controls)
            {
                if (c is Button)
                {
                    c.Text = "0";
                    c.Visible = true;
                }
            }
        }


        private void MostrarBotonAElegir(Button boton, List<int> valores, int posicion ,TextBox tb, BE.JUGADOR j, int numerodado)
        {
            valores[posicion]++;
            if (!tb.Visible && turno.JUGADOR == j)
            {
                boton.Text = (valores[posicion] * numerodado).ToString();
                boton.Visible = true;
            }
            
        }

        public void MostrarOpcionesPuntaje(List<BE.DADO> dados, int tirosRestantes)
        {
            
            var jugadorActivo = new BE.JUGADOR();

            List<int> valores = new List<int>(); //CANTIDAD DE DADOS IGUALES POR NUMERO

            for (int i = 0; i < 6; i++)
            {
                valores.Add(0);
            }
            
          
            if (turno.JUGADOR == jugadores[0])
            {
                jugadorActivo = jugadores[0];
            }
            else
            {
                jugadorActivo = jugadores[1];
            }

            EsconderBotonesUsados(jugadorActivo);
            
            foreach (BE.DADO d in dados)
            {
                switch (d.Valor)
                {
                    case 1:
                        if (jugadorActivo == jugadores[0])
                        {
                            MostrarBotonAElegir(button1, valores, 0, J1P1, jugadores[0], 1);
                        }
                        else
                        {
                            MostrarBotonAElegir(button1, valores, 0, J2P1, jugadores[1], 1);
                        }
                        break;

                    case 2:
                        if (jugadorActivo == jugadores[0])
                        {
                            MostrarBotonAElegir(button2, valores, 1, J1P2, jugadores[0], 2);
                        }
                        else
                        {
                            MostrarBotonAElegir(button2, valores, 1, J2P2, jugadores[1], 2);
                        }
                        break;

                    case 3:
                        if (jugadorActivo == jugadores[0])
                        {
                            MostrarBotonAElegir(button3, valores, 2, J1P3, jugadores[0], 3);
                        }
                        else
                        {
                            MostrarBotonAElegir(button3, valores, 2, J2P3, jugadores[1], 3);
                        }
                        break;

                    case 4:
                        if (jugadorActivo == jugadores[0])
                        {
                            MostrarBotonAElegir(button4, valores, 3, J1P4, jugadores[0], 4);
                        }
                        else
                        {
                            MostrarBotonAElegir(button4, valores, 3, J2P4, jugadores[1], 4);
                        }
                        break;

                    case 5:
                        if (jugadorActivo == jugadores[0])
                        {
                            MostrarBotonAElegir(button5, valores, 4, J1P5, jugadores[0], 5);
                        }
                        else
                        {
                            MostrarBotonAElegir(button5, valores, 4, J2P5, jugadores[1], 5);
                        }
                        break;

                    default:
                        if (jugadorActivo == jugadores[0])
                        {
                            MostrarBotonAElegir(button6, valores, 5, J1P6, jugadores[0], 6);
                        }
                        else
                        {
                            MostrarBotonAElegir(button6, valores, 5, J2P6, jugadores[1], 6);
                        }
                        break;
                }
            }

            switch (BLL.PUNTAJE.PuntoTiro(valores))
            {
                case 0:             //ESCALERA
                    if (tirosRestantes == 2)
                    {
                        buttonEscalera.Text = "25";
                    }
                    else
                    {
                        buttonEscalera.Text = "20";
                    }

                    if (jugadorActivo == jugadores[0])
                    {
                        MostrarBoton(buttonEscalera, J1E);
                    }
                    else
                    {
                        MostrarBoton(buttonEscalera, J2E);
                    }
                    
                    break;

                case 1:
                    if (tirosRestantes == 2)
                    {
                        buttonFull.Text = "35";
                    }
                    else
                    {
                        buttonFull.Text = "30";
                    }

                    if (jugadorActivo == jugadores[0])
                    {
                        MostrarBoton(buttonFull, J1F);
                    }
                    else
                    {
                        MostrarBoton(buttonFull, J2F);
                    }

                    break;

                case 2:
                    if (tirosRestantes == 2)
                    {
                        buttonPoker.Text = "45";
                    }
                    else
                    {
                        buttonPoker.Text = "40";
                    }

                    if (jugadorActivo == jugadores[0])
                    {
                        MostrarBoton(buttonPoker, J1P);
                    }
                    else
                    {
                        MostrarBoton(buttonPoker, J2P);
                    }
                    break;

                case 3:

                    if (jugadorActivo == jugadores[0])
                    {
                        if (J1G.Text == "60")
                        {
                            buttonDobleGenerala.Text = "100";

                            MostrarBoton(buttonDobleGenerala, J1DG);
                        }
                        else
                        {
                            buttonGenerala.Text = "60";

                            MostrarBoton(buttonGenerala, J1G);

                            
                        }
                    }
                    else
                    {
                        if (J2G.Text == "60")
                        {
                            buttonDobleGenerala.Text = "100";

                            MostrarBoton(buttonDobleGenerala, J2DG);
                        }
                        else
                        {
                            buttonGenerala.Text = "60";

                            MostrarBoton(buttonGenerala, J2G);

                            
                        }

                    }

                    
                    
                    break;
                default:
                    break;
            }

            

        }

        private void MostrarBoton(Button boton, TextBox textbox)
        {
            if (!textbox.Visible)
            {
                boton.Visible = true;
            }
        }

        public bool VerificarCompleto()
        {
            
                int cant = 0;

                foreach (Control c in Controls)
                {
                    if (c is TextBox && c.Visible == true)
                    {
                        cant++;
                    }
                }

                if (cant == 22)
                {
                    completo = true;
                    return completo;
                }
                else
                {
                    return false;
                }
            

        }

        #region<Botones>

        private void Botones(int valor, Button boton, TextBox tbj1, TextBox tbj2)
        {
            BE.JUGADOR j;

            if (turno.JUGADOR == jugadores[0])
            {
                tbj1.Text = boton.Text;
                tbj1.Visible = true;
                j = jugadores[0];
                PUNTAJEJ1.Text = (int.Parse(PUNTAJEJ1.Text) + int.Parse(boton.Text)).ToString();
            }
            else
            {
                tbj2.Text = boton.Text;
                tbj2.Visible = true;
                j = jugadores[1];
                PUNTAJEJ2.Text = (int.Parse(PUNTAJEJ2.Text) + int.Parse(boton.Text)).ToString();
            }
            
            EsconderBotones();
            
            BLL.TURNO.TerminaTurno(valor, j, turno, partida);
            terminoTurno = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Botones(1, button1, J1P1, J2P1);
        }


        private void button2_Click(object sender, EventArgs e)
        {
            Botones(2, button2, J1P2, J2P2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Botones(3, button3, J1P3, J2P3);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Botones(4, button4, J1P4, J2P4);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Botones(5, button5, J1P5, J2P5);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Botones(6, button6, J1P6, J2P6);
        }

        private void buttonEscalera_Click(object sender, EventArgs e)
        {
            Botones(7, buttonEscalera, J1E, J2E);
        }

        private void buttonFull_Click(object sender, EventArgs e)
        {
            Botones(8, buttonFull, J1F, J2F);
        }

        private void buttonPoker_Click(object sender, EventArgs e)
        {
            Botones(9, buttonPoker, J1P, J2P);
        }

        private void buttonGenerala_Click(object sender, EventArgs e)
        {
            Botones(10, buttonGenerala, J1G, J2G);
        }

        private void buttonDobleGenerala_Click(object sender, EventArgs e)
        {
            Botones(11, buttonDobleGenerala, J1DG, J2DG);
        }

        

        private void EsconderBotonesUsados(BE.JUGADOR jugador)
        {
            if (jugador == jugadores[0])
            {
                if (J1P1.Visible) { button1.Visible = false; }
                if (J1P2.Visible) { button2.Visible = false; }
                if (J1P3.Visible) { button3.Visible = false; }
                if (J1P4.Visible) { button4.Visible = false; }
                if (J1P5.Visible) { button5.Visible = false; }
                if (J1P6.Visible) { button6.Visible = false; }
                if (J1E.Visible) { buttonEscalera.Visible = false; }
                if (J1F.Visible) { buttonFull.Visible = false; }
                if (J1P.Visible) { buttonPoker.Visible = false; }
                if (J1G.Visible) { buttonGenerala.Visible = false; }
                if (J1DG.Visible) { buttonDobleGenerala.Visible = false; }
            }
            else
            {
                if (J2P1.Visible) { button1.Visible = false; }
                if (J2P2.Visible) { button2.Visible = false; }
                if (J2P3.Visible) { button3.Visible = false; }
                if (J2P4.Visible) { button4.Visible = false; }
                if (J2P5.Visible) { button5.Visible = false; }
                if (J2P6.Visible) { button6.Visible = false; }
                if (J2E.Visible) { buttonEscalera.Visible = false; }
                if (J2F.Visible) { buttonFull.Visible = false; }
                if (J2P.Visible) { buttonPoker.Visible = false; }
                if (J2G.Visible) { buttonGenerala.Visible = false; }
                if (J2DG.Visible) { buttonDobleGenerala.Visible = false; }
            }
        }


        #endregion

        
    }
}
