﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using BE;

namespace GENERALA
{
    public partial class IniciarSesion : UserControl
    {
        public IniciarSesion()
        {
            InitializeComponent();
        }

        private List<BE.JUGADOR> jugadores;

        public List<BE.JUGADOR> Jugadores
        {
            get { return jugadores; }
            set { jugadores = value; }
        }



        private void button1_Click(object sender, EventArgs e)
        {
            bool repetido = false;

            if (Jugadores == null)
            {
                jugadores = new List<BE.JUGADOR>();
            }


            if(jugadores != null) 
            {
                foreach (BE.JUGADOR jug in jugadores)
                {
                    if (jug.Nombre == textBox1.Text)
                    {
                        repetido = true;
                    }
                }

                if (jugadores.Count < 2 && repetido == false)
                {
                    BE.JUGADOR j = BLL.JUGADOR.IniciarSesion(textBox1.Text, textBox2.Text);

                    if (j == null)
                    {
                        MessageBox.Show("La contraseña o el usuario son incorrectos");
                    }
                    else
                    {
                        jugadores.Add(j);
                        Limpiar(false);
                    }
                }
                else
                {
                    switch (repetido)
                    {
                        case true: MessageBox.Show("El usuario ya inició sesión");
                            break;
                                                    
                        default: MessageBox.Show("Solo se permiten partidas de 2 jugadores");
                            break;
                    }
                    
                }
            }
            
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<BE.JUGADOR> jugadores = BLL.JUGADOR.ListarJugadores();
            bool disponible = true;
            BLL.JUGADOR j = new BLL.JUGADOR();

            foreach (BE.JUGADOR jug in jugadores)
            {
                if (jug.Nombre == textBox1.Text)
                {
                    MessageBox.Show("El nombre de usaurio no está disponible");
                    disponible = false;
                }
            }

            if (disponible)
            {
                j.RegistrarJugador(textBox1.Text, textBox2.Text);
            }

            Limpiar(false);
        }

        public void Limpiar(bool jug)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            if (jug)
            {   
                jugadores.Clear();
            }
            
        }
    }
}
