﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BLL
{
    public class JUGADOR
    {

        public delegate void delEnviarDado(List<BE.DADO> d);

        public event delEnviarDado EnviarDado;

        MP_JUGADOR jugador = new MP_JUGADOR();

        public void Tirar()
        {
            List<BE.DADO> dados = BE.TABLERO.Dados;

            List<BE.DADO> dadosNuevos = new List<BE.DADO>();

            foreach (BE.DADO d in dados)
            {
                if (d.TiraDeNuevo)
                {
                    dadosNuevos.Add(d);
                }
            }


            if (dadosNuevos.Count == 0 || dadosNuevos.Count == 5)
            {
                dados.Clear();
                int ID = 1;

                for (int da = 2; da <= 6; da++)
                {
                    BE.POSICION pos = new BE.POSICION(da, 10);

                    BE.DADO d = new BE.DADO();
                    d.ID = ID;
                    d.Valor = BE.TABLERO.random.Next(1, 7);
                    d.POSICION = pos;
                    d.TiraDeNuevo = false;
                    dados.Add(d);
                    ID++;
                }

                
            }
            else
            {
                foreach (BE.DADO d in dadosNuevos)
                {
                    BE.DADO dadonuevo = (from BE.DADO dad in dados
                                        where d == dad
                                        select dad
                                        ).FirstOrDefault();

                    dadonuevo.Valor = BE.TABLERO.random.Next(1, 7);
                    dadonuevo.TiraDeNuevo = false;

                }
            }

            BE.TABLERO.Dados = dados;

            this.EnviarDado(BE.TABLERO.Dados);
        }

        static public BE.JUGADOR IniciarSesion(string usuario, string clave)
        {
            return MP_JUGADOR.IniciarSesion(usuario, clave);
        }

        static public List<BE.JUGADOR> ListarJugadores()
        {
            return MP_JUGADOR.ListarJugadores();
        }

        public void RegistrarJugador(string nombre, string clave)
        {
            jugador.RegistrarNuevoJugador(nombre, clave);
        }

        static public List<int> PartidasGanadasEmpatadasPerdidas(BE.JUGADOR j)
        {
            List<int> g_e_p = new List<int>();

            g_e_p.Add(0);
            g_e_p.Add(0);
            g_e_p.Add(0);
            g_e_p.Add(0);

            List<BE.PARTIDA> partidas = BLL.PARTIDA.ListaPartidas();

            foreach (BE.PARTIDA p in partidas)
            {
                if (p.Jugadores.Count == 2)
                {
                    if ((p.Jugadores[0].id == j.id || p.Jugadores[1].id == j.id))
                    {
                        if (p.Ganador.id == j.id)
                        {
                            g_e_p[0]++;
                        }
                        else if (p.Ganador == null)
                        {
                            g_e_p[1]++;
                        }
                        else
                        {
                            g_e_p[2]++;
                        }
                    }
                }
                
            }

            int total = 0;

            foreach (int i in g_e_p)
            {
                total += i;
            }

            PorcentajeGanadas(j, g_e_p, total);

            g_e_p[3] = total;

            return g_e_p;
        }

        static private void PorcentajeGanadas(BE.JUGADOR j, List<int> est, int total)
        {
            if (total != 0)
            {
                j.Porcentaje_Ganadas = est[0] * 100 / total;
            }
            else
            {
                j.Porcentaje_Ganadas = 0;
            }
        }

        public void ModificarTiempo(BE.JUGADOR jug)
        {
            jugador.ModificarTiempo(jug);
        }
    }
}