﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BLL
{
    public class PARTIDA
    {
        MP_PARTIDA partida = new MP_PARTIDA();

        public void AltaPartida(BE.PARTIDA p)
        {
            partida.NuevaPartida(p);
        }

        public void FinalPartida(BE.PARTIDA p)
        {
            partida.FinalPartida(p);
        }

        public void BorrarUltimaPartida()
        {
            partida.BorrarUltimaPartida();
        }

        static public List<BE.PARTIDA> ListaPartidas()
        {
            return MP_PARTIDA.ListarPartidas();
        }
    }
}