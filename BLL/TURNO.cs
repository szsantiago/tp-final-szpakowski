﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE;

namespace BLL
{
    public class TURNO
    {
        


        static public void TerminaTurno(int puntajeelegido, BE.JUGADOR j, BE.TURNO t, BE.PARTIDA part)
        {
            
            BLL.PUNTAJE PUNTAJE = new PUNTAJE();
            BE.PUNTAJE p = PUNTAJE.EstablecerPuntaje(puntajeelegido, TABLERO.Dados, 3 - t.TirosRestantes);
            j.Puntos +=  p.Valor;
            t.TirosRestantes = 0;
            
        }
    }
}