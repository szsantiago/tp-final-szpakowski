﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BE;

namespace BLL
{
    public class PUNTAJE
    {
        BE.PUNTAJE p = new BE.PUNTAJE();


        static public int PuntoTiro(List<int> valores)
        {
            int escalera = 0;

            foreach (int i in valores)              
            {
                if (i == 5)                    //GENERALA
                {
                    return 3;       //GENERALA
                }
                else if (i == 4)                    //POOKER
                {
                    return 2;           //POKER
                }
                else if (i == 3)
                {
                    foreach (int x in valores)
                    {
                        if (x == 2)
                        {
                            return 1;   //FULL
                        }
                    }
                }
                else if (i == 1)
                {
                    escalera++;
                } 
            }

            if (escalera == 5)
            {
                if (valores[0] == 0 || valores[5] == 0)
                {
                    return 0;           //ESCALERA
                }
            }

            return 4;
        }


        internal BE.PUNTAJE EstablecerPuntaje(int valor, List<DADO> dados, int cantidadTiros)
        {
            
            switch (valor)
            {
                case 1:
                    p.Descripcion = "1";
                    Calcular(1, dados);
                    break;

                case 2:
                    p.Descripcion = "2";
                    Calcular(2, dados);
                    break;

                case 3:
                    p.Descripcion = "3";
                    Calcular(3, dados);
                    break;

                case 4:
                    p.Descripcion = "4";
                    Calcular(4, dados);
                    break;

                case 5:
                    p.Descripcion = "5";
                    Calcular(5, dados);
                    break;

                case 6:
                    p.Descripcion = "6";
                    Calcular(6, dados);
                    break;

                case 7:
                    p.Descripcion = "ESCALERA";
                    Calcular(20, cantidadTiros);
                    break;

                case 8:
                    p.Descripcion = "FULL";
                    Calcular(30, cantidadTiros);
                    break;

                case 9:
                    p.Descripcion = "POKER";
                    Calcular(40, cantidadTiros);
                    break;

                case 10:
                    p.Descripcion = "GENERALA";
                    Calcular(60, cantidadTiros);
                    break;

                default:
                    p.Descripcion = "DOBLE GENERALA";
                    Calcular(100, cantidadTiros);
                    break;
                
            }

            return p;
        }


        private void Calcular(int numero, List<DADO> dados)
        {
            foreach (DADO d in dados)
            {
                if (d.Valor == numero)
                {
                    p.Valor += d.Valor;
                }
            }
        }

        private void Calcular(int v, int cantTiros)
        {
            if (cantTiros == 1)
            {
                p.Valor = v + 5; 
            }
        }


    }
}